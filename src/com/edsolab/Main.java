package com.edsolab;

import java.util.Random;
import java.util.Scanner;

public class Main {
    static Random random = new Random();
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int n = 0;
        try {
            do {
                System.out.println("Nhâp n: ");
                n = sc.nextInt();
                sc.nextLine();
            } while (n <= 0);
        } catch (Exception e) {
            System.out.println("Nhập sai định dạng số");
        } finally {
        }
        double[] array = create(n);
        System.out.println("Mảng tạo là: ");
        out(array, n);
        System.out.println();
        System.out.println("Mảng sau khi xóa:");
        delete(array,n);
    }

    public static double[] create(int n) {
        double[] arrayDouble = new double[n];
        for (int i = 0; i < n; i++){
            arrayDouble[i] = random.nextDouble() * 100;
        }
        return arrayDouble;
    }

    public static void out(double[] arr, int n){
        for (int i = 0; i < n; i++){
            System.out.print(arr[i] + " ");
        }
    }

    public static void delete(double[] arr, int n){
        for (int i = 0; i < n; i++){
            if(arr[i] < 50){
                System.out.println("Mày sắp bị xóa: " + arr[i]);
                for (int j = i; j < n-1; j++){
                    arr[j] = arr[j+1];
                }
                n--;
                i--;
            }
        }
        out(arr,n);
    }
}
