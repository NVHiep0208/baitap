package Bai47;

import java.util.ArrayList;
import java.util.List;

public class Monster implements ISaveable{

    private String name;
    private int hitPoints;
    private int strength;

    public Monster(String name, int hitPoints, int strength) {
        this.name = name;
        this.hitPoints = hitPoints;
        this.strength = strength;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public void setHitPoints(int hitPoints) {
        this.hitPoints = hitPoints;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    @Override
    public List<String> write() {
        List<String> stringList = new ArrayList<>();
        stringList.add(0,this.name);
        stringList.add(1,this.hitPoints+"");
        stringList.add(2,this.strength+"");
        return stringList;
    }

    @Override
    public void read(List<String> listValue) {
        if(listValue.size() != 0 && listValue != null){
            this.name = listValue.get(0);
            this.hitPoints = Integer.parseInt(listValue.get(1));
            this.strength = Integer.parseInt(listValue.get(2));
        }
    }

    @Override
    public String toString() {
        return "Monster{" +
                "name='" + name + '"' +
                ", hitPoints=" + hitPoints +
                ", strength=" + strength +
                '}';
    }
}
