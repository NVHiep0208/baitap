package Bai46;
import java.util.ArrayList;
import java.util.LinkedList;
public class Album {
    private String name;
    private String artist;
    private ArrayList<Song> songs;

    public Album(String name, String artist) {
        this.name = name;
        this.artist = artist;
        this.songs = new ArrayList<Song>();
    }

    public boolean addSong(String title, double duration) {
        if(this.findSong(title) == null){
            this.songs.add(new Song(title, duration));
            return true;
        }
        return false;
    }

    private Song findSong(String title) {
        for (Song s : songs) {
            if (s.getTitle().equals(title))
                return s;
        }
        return null;
    }

    public boolean addToPlayList(int track, LinkedList<Song> playlist) {
        if (track < 0 || track > songs.size()) {
            return false;
        }
        Song song = songs.get(track - 1);
        boolean isAdd = playlist.add(song);
        if (isAdd) {
            return true;
        }
        return false;
    }

    public boolean addToPlayList(String title, LinkedList<Song> playlist) {
        Song song = findSong(title);
        if (song == null) {
            return false;
        }
        boolean isAdd = playlist.add(song);
        if (isAdd) {
            return true;
        }
        return false;
    }
}

